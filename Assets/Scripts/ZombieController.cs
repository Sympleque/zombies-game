using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieController : MonoBehaviour
{
    public float lookRadius = 8f;

    Transform target;
    NavMeshAgent agent;
    AudioSource groan;
    Animator animator;

	private int inAttackRangeAnimID;
	private int seesTargetAnimID;
	private int blendAnimID;
	private int movementAnimID;

    void Start()
    {
        target = PlayerManager.Instance.player.transform;
        agent = GetComponent<NavMeshAgent>();
        groan = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();

		inAttackRangeAnimID = Animator.StringToHash("InAttackRange");
		seesTargetAnimID = Animator.StringToHash("SeesTarget");
		blendAnimID = Animator.StringToHash("Blend");
		movementAnimID = Animator.StringToHash("Movement");

		animator.SetFloat(blendAnimID, 0.5f);
    }

    void Update()
    {
        // get distance between zombie and player
        float distance = Vector3.Distance(target.position, transform.position);

		bool seesTarget = distance <= lookRadius;
		animator.SetBool(seesTargetAnimID, seesTarget);

		if (!seesTarget && groan.isPlaying)
			groan.Stop();

		// We check if the zombie is in the movement state, that way it waits for the scream to finish.
		// May not be the best solution but it works.
		//
        if (animator.GetCurrentAnimatorStateInfo(0).shortNameHash == movementAnimID)
        {
            // player is within range of zombie's view. make zombie scream
            agent.SetDestination(target.position);
        }
        else
        {
            // player isn't within range of zombie's view, stay put and dance
            agent.SetDestination(transform.position);
        }

		bool inRange = distance <= agent.stoppingDistance;
		animator.SetBool(inAttackRangeAnimID, inRange);

        if (inRange)
        {
			// zombie has reached the player, attack them
            FaceTarget();
        }
    }

	private void AnimEvent_Scream()
	{
		groan.Play();
	}

    void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
