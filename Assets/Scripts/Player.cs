
using UnityEngine;

public class Player : MonoBehaviour
{
    public float baseSpeed;
    public float runningAmplifier;

	[Tooltip("Speed change rate for animation blending.")]
	public float speedChangeRate;

    public bool lockCursor;

    private CharacterController characterController;
    private Animator animator;

	private int leftAndRightAnimID;
	private int forwardAndBackwardAnimID;

	private float animationBlendX;
	private float animationBlendZ;

    private PlayerMovementInfo playerMovement;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();

        playerMovement = new PlayerMovementInfo();
        playerMovement.baseSpeed = baseSpeed;
        playerMovement.runningAmplifier = runningAmplifier;

		leftAndRightAnimID = Animator.StringToHash("leftAndRight");
		forwardAndBackwardAnimID = Animator.StringToHash("forwardAndBackward");

        if (lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    void Update()
    {
        ProcessInput();

        CalculateDirectionAndDistance();
        PerformPhysicalMovement();

        PerformBlendTreeAnimation();
    }

	void LateUpdate()
	{
        RotatePlayerWithCamera();
	}

    public void ProcessInput()
    {
        playerMovement.leftAndRight = Input.GetAxis("Horizontal"); // a and d
        playerMovement.forwardAndBackward = Input.GetAxis("Vertical"); // w and s. range -1...1

        playerMovement.movingForwards = playerMovement.forwardAndBackward > 0.0f;
        playerMovement.movingBackwards = playerMovement.forwardAndBackward < 0.0f;

        bool running = Input.GetKey(KeyCode.LeftShift) && !playerMovement.movingBackwards;

        if (running)
        {
            playerMovement.speed = playerMovement.baseSpeed * playerMovement.runningAmplifier;
        }
        else
        {
            playerMovement.speed = baseSpeed;

            playerMovement.leftAndRight = playerMovement.leftAndRight / 2.0f;
            playerMovement.forwardAndBackward = playerMovement.forwardAndBackward / 2.0f;
        }
    }

	public void PerformBlendTreeAnimation()
    {
		animationBlendX = Mathf.Lerp(animationBlendX, playerMovement.leftAndRight, Time.deltaTime * speedChangeRate);
        animator.SetFloat(leftAndRightAnimID, animationBlendX);

		animationBlendZ = Mathf.Lerp(animationBlendZ, playerMovement.forwardAndBackward, Time.deltaTime * speedChangeRate);
        animator.SetFloat(forwardAndBackwardAnimID, animationBlendZ);

		animator.speed = playerMovement.baseSpeed / 2f;
    }

    public void CalculateDirectionAndDistance()
    {
        Vector3 moveDirectionForward = transform.forward * playerMovement.forwardAndBackward;
        Vector3 moveDirectionSide = transform.right * playerMovement.leftAndRight;

        playerMovement.direction = moveDirectionForward + moveDirectionSide;
        playerMovement.normalizedDirection = playerMovement.direction.normalized;

        playerMovement.distance = (playerMovement.normalizedDirection * playerMovement.speed + Physics.gravity) * Time.deltaTime;
    }

    public void PerformPhysicalMovement()
    {
        characterController.Move(playerMovement.distance);
    }

    public void RotatePlayerWithCamera()
    {
        Vector3 rotation;
        if (Input.GetKey(KeyCode.T))
        {
            return; // do nothing
        }
        else
        {
            rotation = Camera.main.transform.eulerAngles;
            rotation.x = 0;
            rotation.z = 0;

            transform.eulerAngles = rotation;
        }
    }




}
